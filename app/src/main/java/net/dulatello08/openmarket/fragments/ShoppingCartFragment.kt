package net.dulatello08.openmarket.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.api.FirebaseCalls

class ShoppingCartFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mFirebaseCalls = FirebaseCalls()
        val view = inflater.inflate(R.layout.fragment_shopping_cart, container, false)
        // Inflate the layout for this fragment
        view.findViewById<Button>(R.id.button).setOnClickListener{
            mFirebaseCalls.sendCart(1, "FZnt3fuH7sOHiSeM6hPj")
            var userCart: String
            mFirebaseCalls.getCartInStr(1){
                userCart = it
                Log.d("UserCart ->", userCart)
            }
        }
        return view
    }
}