package net.dulatello08.openmarket.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.activities.SHARED_PREFERENCE_NAME
import net.dulatello08.openmarket.adapters.AddAnnouncementPhotosAdapter
import net.dulatello08.openmarket.adapters.MainListModel
import net.dulatello08.openmarket.api.FirebaseCalls
import net.dulatello08.openmarket.api.RandomCalls
import java.util.*

class AddAnnouncementFragment : Fragment() {
    private lateinit var publishAnnouncement: Button
    private lateinit var galleryPhotos: ImageView
    private lateinit var cameraPhoto: ImageView
    private lateinit var textHeader: EditText
    private lateinit var textDescription: EditText
    private lateinit var progressBar: FrameLayout
    private lateinit var recyclerAddedPhoto: RecyclerView
    private lateinit var currencySpinner: Spinner
    private lateinit var announcementPrice: EditText
    private val CAMERA_INTENT_CODE = 34
    private val uriArrayList = arrayListOf<String>()
    private val GALLERY_INTENT_CODE = 76
    private val mFirebaseCalls = FirebaseCalls()
    private val mAdapter =
        AddAnnouncementPhotosAdapter(object : AddAnnouncementPhotosAdapter.onClickdial {
            override fun clickDelete(position: Int) {
                uriArrayList.removeAt(position)
            }
        })
    private val mRandomCalls = RandomCalls()
    private val permRequest =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            if (it[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_INTENT_CODE)
            } else {
                Toast.makeText(
                    requireContext(),
                    "You should grant the permission in order to use camera",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    private var mInterstitialAd: InterstitialAd? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        publishAnnouncement = view.findViewById(R.id.addAnnouncementPublish)
        galleryPhotos = view.findViewById(R.id.addAnnouncementGallery)
        cameraPhoto = view.findViewById(R.id.addAnnouncementCamera)
        progressBar = requireActivity().findViewById(R.id.progressbar)
        textDescription = view.findViewById(R.id.addAnnouncementDescription)
        textHeader = view.findViewById(R.id.addAnnouncementHeader)
        announcementPrice = view.findViewById(R.id.addAnnouncementPrice)
        val spinnerAdapter = ArrayAdapter<Any>(
            requireContext(),
            R.layout.currency_spinner_item,
            resources.getStringArray(R.array.currency_types)
        )
        currencySpinner = view.findViewById(R.id.currency_spinner)
        currencySpinner.adapter = spinnerAdapter
        recyclerAddedPhoto = view.findViewById(R.id.addAnnouncementPhotoList)
        recyclerAddedPhoto.layoutManager = GridLayoutManager(requireContext(), 2)
        recyclerAddedPhoto.adapter = mAdapter
        galleryPhotos.setOnClickListener {
            val galleryIntent = Intent()
            galleryIntent.type = "image/*"
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(galleryIntent, GALLERY_INTENT_CODE)
        }
        //Open Camera
        cameraPhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 23) {
                if (requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    permRequest.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                } else {
                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(cameraIntent, CAMERA_INTENT_CODE)
                }
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_INTENT_CODE)
            }
        }
        //firebase sending
        Firebase.firestore.collection("Announcements").orderBy("Date").limit(50).get()
        publishAnnouncement.setOnClickListener { view ->
            if (textHeader.text.toString().isNotBlank() && textDescription.text.toString()
                    .isNotBlank() && announcementPrice.text.toString().isNotBlank()
            ) {
                progressBar.visibility = View.VISIBLE
                requireActivity().window.setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                )
                val sharedPref = requireContext().getSharedPreferences(
                    SHARED_PREFERENCE_NAME,
                    Context.MODE_PRIVATE
                )
                val addModel = MainListModel(
                    announcementName = textHeader.text.toString(),
                    announcementDate = GregorianCalendar(TimeZone.getTimeZone("gmt")).time,
                    announcementDescription = textDescription.text.toString(),
                    announcementPhotoUris = uriArrayList,
                    announcementUserId = sharedPref.getInt("id", -1),
                    announcementPrice = announcementPrice.text.toString().toDouble(),
                    announcementCurrency = currencySpinner.selectedItemPosition
                )
                mFirebaseCalls.putAnnouncement(addModel)
                    .addOnSuccessListener {
                        if (uriArrayList.size > 0) {
                            val uriOfString = arrayListOf<Uri>()
                            uriArrayList.forEach { string -> uriOfString.add(Uri.parse(string)) }
                            mFirebaseCalls.putImgFromUri(uriOfString, it.id) {
                                progressBar.visibility = View.GONE
                                requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                                requireActivity().findViewById<BottomNavigationView>(R.id.bottomNavView).selectedItemId =
                                    R.id.navigation_home
                                Toast.makeText(
                                    requireContext(),
                                    "You successfully published your announcement",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            progressBar.visibility = View.GONE
                            requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                            requireActivity().findViewById<BottomNavigationView>(R.id.bottomNavView).selectedItemId =
                                R.id.navigation_home
                            Toast.makeText(
                                requireContext(),
                                "You successfully published your announcement",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        recreate()
                    }
            } else Toast.makeText(
                requireContext(),
                "You should write the header of announcement",
                Toast.LENGTH_SHORT
            ).show()
            if (mInterstitialAd != null && mRandomCalls.getRandomBoolean(2)) {
                mInterstitialAd?.show(requireActivity())
            }
        }
    }

    private fun recreate() {
        uriArrayList.clear()
        textDescription.setText("")
        textHeader.setText("")
        mAdapter.setData(arrayListOf())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == GALLERY_INTENT_CODE) {
            val mClipData = data?.clipData
            val mData = data?.data.toString()
            if (null != mClipData) {
                for (el in 0 until mClipData.itemCount) {
                    uriArrayList.add(mClipData.getItemAt(el).uri.toString())
                }
                mAdapter.setData(uriArrayList)
            } else {
                uriArrayList.add(mData)
                mAdapter.setData(uriArrayList)
            }
        } else if (resultCode == RESULT_OK && requestCode == CAMERA_INTENT_CODE) {
            if (data != null) {
                val bit =
                    Bitmap.createScaledBitmap(
                        data.extras?.get("data") as Bitmap,
                        1000,
                        1000,
                        true
                    )
                val path = MediaStore.Images.Media.insertImage(
                    requireContext().contentResolver,
                    bit,
                    "Title",
                    null
                )
                uriArrayList.add(path)
                mAdapter.setData(uriArrayList)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_announcement, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            requireActivity(),
            "ca-app-pub-7509088958653785/3227241751",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                }
            })
        mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
            }

            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
            }

            override fun onAdShowedFullScreenContent() {
                mInterstitialAd = null
            }
        }
    }
}