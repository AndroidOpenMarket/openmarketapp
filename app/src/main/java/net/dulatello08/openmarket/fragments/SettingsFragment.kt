package net.dulatello08.openmarket.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.activities.MainActivity
import net.dulatello08.openmarket.activities.SHARED_PREFERENCE_NAME

class SettingsFragment : Fragment() {
    private lateinit var userAnnouncementBut: Button
    private lateinit var themeButton: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAnnouncementBut = view.findViewById(R.id.user_announcement_button)
        themeButton = view.findViewById(R.id.application_theme_button)
        val themeDialog = themeDialog(0)
        val langDialog = themeDialog(1)
        userAnnouncementBut.setOnClickListener {
            langDialog?.show()
        }
        themeButton.setOnClickListener {
            themeDialog?.show()
        }
    }

    private fun themeDialog(dialogType: Int): AlertDialog? {
        val shr =
            requireContext().getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        val array: Array<String> = when (dialogType) {
            0 -> resources.getStringArray(R.array.theme)
            1 -> resources.getStringArray(R.array.languages)
            else -> arrayOf("ERROR")
        }
        val preferenceName: String = when (dialogType) {
            0 -> "THEME"
            1 -> "LANG"
            else -> "ERROR"
        }
        val builder = AlertDialog.Builder(requireContext())
        val inflater = requireActivity().layoutInflater
        val dialogView = inflater.inflate(R.layout.select_theme_dialog, null)
        val radioGroup = dialogView.findViewById<RadioGroup>(R.id.theme_radio_group)
        dialogView.findViewById<TextView>(R.id.select_theme_theme).text = when(dialogType){
            0->getString(R.string.select_theme_text_string)
            1->getString(R.string.select_theme_text_string)
            else -> "ERROR"
        }
        val themeId = requireActivity().packageManager.getActivityInfo(
            requireActivity().componentName,
            0
        ).theme
        val ta = requireActivity().obtainStyledAttributes(
            themeId,
            intArrayOf(R.attr.firstColor, R.attr.secondColor)
        )
        for (theme in array) {
            val radioButton = RadioButton(requireContext())
            radioButton.text = theme
            radioButton.textSize = 20F
            radioButton.setPadding(16)
            radioButton.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            if (Build.VERSION.SDK_INT >= 21) {
                radioButton.buttonTintList = ColorStateList(
                    arrayOf(
                        intArrayOf(-android.R.attr.state_enabled),
                        intArrayOf(android.R.attr.state_enabled)
                    ), intArrayOf(Color.GRAY, ta.getColor(0, Color.YELLOW))
                )
            }
            if (theme == array[shr.getInt(preferenceName, 0)])
                radioButton.isChecked = true
            radioGroup.addView(radioButton)
        }
        radioGroup.setOnCheckedChangeListener { rdg: RadioGroup, i: Int ->
            val selectedButton = rdg.findViewById<RadioButton>(rdg.checkedRadioButtonId)
            shr.edit().putInt(preferenceName, rdg.indexOfChild(selectedButton)).apply()
            val resIntent = Intent(requireContext(), MainActivity::class.java)
            resIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(resIntent)
        }
        ta.recycle()
        builder.setView(dialogView)
        return builder.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }
}