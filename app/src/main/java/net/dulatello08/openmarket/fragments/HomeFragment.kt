package net.dulatello08.openmarket.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.adapters.MainListAdapter
import net.dulatello08.openmarket.api.FirebaseCalls

class HomeFragment : Fragment() {
    private lateinit var mainList: RecyclerView
    private lateinit var mSwipe: SwipeRefreshLayout
    private lateinit var addBanner: AdView
    private val mAdapter = MainListAdapter()
    private val mFirebaseCalls = FirebaseCalls()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainList = view.findViewById(R.id.mainList)
        mSwipe = view.findViewById(R.id.mainListRefresh)
        addBanner = view.findViewById(R.id.mainListBanner)
        val adRequest = AdRequest.Builder().build()
        addBanner.loadAd(adRequest)
        mainList.layoutManager = StaggeredGridLayoutManager(
            2, RecyclerView.VERTICAL
        )
        mainList.adapter = mAdapter
        mFirebaseCalls.getSomeAnnouncements(50) { model ->
            mAdapter.setMainData(model)
            view.findViewById<FrameLayout>(R.id.progressbar)

        }
        mSwipe.setOnRefreshListener {
            mFirebaseCalls.getSomeAnnouncements(10) { model ->
                mAdapter.setMainData(model)
                mSwipe.isRefreshing = false
            }
        }
    }

    override fun onStop() {
        super.onStop()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    fun search(term: String) {
        if (term.isNotBlank()) {
            Log.d("MyTag",term)
            mFirebaseCalls.getAnnouncementsBySearchTerm(term, true, 10) {
                mAdapter.setMainData(it)
            }
        }
    }
}