package net.dulatello08.openmarket.api

import java.util.Random

open class RandomCalls {
    open fun getRandomBoolean(salting: Int) :Boolean{
        //idk random
        // create random object
        val randomNo = Random()
        // get next next boolean value
        var value: Boolean
        value = randomNo.nextBoolean()
        for(i in 0 until salting) {
            value = randomNo.nextBoolean()
        }
        return value
    }
}