package net.dulatello08.openmarket.api

class ErrNoConnectionException(message: String) : Exception(message)
