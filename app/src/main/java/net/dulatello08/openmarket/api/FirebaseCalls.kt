package net.dulatello08.openmarket.api

import android.net.Uri
import android.util.Log
import androidx.annotation.NonNull
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import net.dulatello08.openmarket.adapters.MainListModel
import java.util.*


open class FirebaseCalls {
    private val modelListModel = arrayListOf<MainListModel>()
    private val updateUris = arrayListOf<String>()
    open fun putImgFromUri(uris: ArrayList<Uri>, docId: String, lambda: () -> Unit) {
        val firestoreRef = Firebase.firestore.collection("Announcements").document(docId)
        val storageRef = FirebaseStorage.getInstance().reference
        firestoreRef.update(mapOf("UriArray" to arrayListOf<String>()))
        for (i in uris.indices) {
            val storageReference = storageRef.child("images/$docId/photo$i")
            storageReference.putFile(uris[i]).addOnSuccessListener { _ ->
                storageReference.downloadUrl.addOnSuccessListener {
                    updateUris.add(it.toString())
                    if (i == uris.size - 1) {
                        firestoreRef.update(mapOf("UriArray" to updateUris))
                            .addOnSuccessListener {
                                updateUris.clear()
                                lambda()
                            }
                    }
                }
            }
        }
    }

    open fun putAnnouncement(@NonNull model: MainListModel): Task<DocumentReference> {
        val map = hashMapOf<String, Any>()
        map["Name"] = model.announcementName
        map["Description"] = model.announcementDescription
        map["Date"] = model.announcementDate
        map["UserId"] = model.announcementUserId
        map["Price"] = model.announcementPrice
        map["Currency"] = model.announcementCurrency
        val db = Firebase.firestore
        return db.collection("Announcements")
            .add(map)
    }

    open fun getSomeAnnouncements(number: Long, lambda: (ArrayList<MainListModel>) -> Unit) {
        Firebase.firestore.collection("Announcements").orderBy("Date").limit(number)
            .get().addOnSuccessListener { documents ->
                for (doc in documents.documents.reversed()) {
                    modelListModel.add(
                        MainListModel(
                            announcementName = doc.get("Name").toString(),
                            announcementDate = doc.getDate("Date") ?: Date(),
                            announcementDescription = doc.get("Description").toString(),
                            announcementPhotoUris = doc.get("UriArray") as? ArrayList<String>
                                ?: arrayListOf(),
                            announcementUserId = doc.getLong("UserId")?.toInt() ?: -1,
                            announcementCurrency = doc.get("Currency")?.toString()?.toInt() ?: 0,
                            announcementPrice = doc.getDouble("Price") ?: -1.0,
                            announcementId = doc.id
                        )
                    )
                }
                lambda(modelListModel)
                modelListModel.clear()
            }
    }

    open fun getAnnouncementsBySearchTerm(
        term: String,
        limit: Boolean,
        limNumber: Long?,
        lambda: (ArrayList<MainListModel>) -> Unit
    ) {
        val db = Firebase.firestore
        val query: Query = if (limit) {
            db.collection("Announcements").whereEqualTo("Name", term).limit(limNumber!!)
        } else {
            db.collection("Announcements").whereEqualTo("Name", term)
        }
        query.get().addOnSuccessListener { documents ->
            for (doc in documents.documents.reversed()) {
                Log.d("MyTag", "Query start")
                modelListModel.add(
                    MainListModel(
                        announcementName = doc.get("Name").toString(),
                        announcementDate = doc.getDate("Date") ?: Date(),
                        announcementDescription = doc.get("Description").toString(),
                        announcementPhotoUris = doc.get("UriArray") as ArrayList<String>,
                        announcementUserId = doc.getLong("UserId")?.toInt() ?: -1,
                        announcementCurrency = doc.get("Currency")?.toString()?.toInt()!!,
                        announcementPrice = doc.getDouble("Price") ?: -1.0,
                    )
                )
            }
            lambda(modelListModel)
            modelListModel.clear()
        }
    }
    open fun sendCart(userId: Int, docName: String) {
        val db = Firebase.firestore
        val userDocumentSnapshot = db.collection("Users").document("$userId")
        userDocumentSnapshot.update("shop_cart", FieldValue.arrayUnion(docName))
    }
    open fun getCart(userId: Int): ArrayList<String> {
        val db = Firebase.firestore
        var docData = arrayListOf("")
        val userDocumentSnapshot = db.collection("Users").document("$userId")
        userDocumentSnapshot.get()
            .addOnSuccessListener {
                docData = it.get("shop_cart") as ArrayList<String>
            }
        return docData
    }
    @Deprecated("Returns to string")
    open fun getCartInStr(userId: Int, lambda: (value: String) -> Unit){
        val db = Firebase.firestore
        var docData = ""
        val userDocumentSnapshot = db.collection("Users").document("$userId")
        userDocumentSnapshot.get()
            .addOnSuccessListener {
                docData = it.get("shop_cart").toString()
                lambda(docData)
            }
    }
    @Deprecated("Too much hard to use")
    open fun getDocById(id: String, lambda: (MainListModel) -> Unit) {
        val db = Firebase.firestore
        val docSnap = db.collection("Announcements").document(id)
        docSnap.get().addOnSuccessListener {doc ->
                val returnData = MainListModel(
                   announcementName = doc.get("Name").toString(),
                   announcementDate = doc.getDate("Date") ?: Date(),
                   announcementDescription = doc.get("Description").toString(),
                   announcementPhotoUris = doc.get("UriArray") as ArrayList<String>,
                   announcementUserId = doc.getLong("UserId")?.toInt() ?: -1,
                   announcementCurrency = doc.get("Currency")?.toString()?.toInt()!!,
                   announcementPrice = doc.getDouble("Price") ?: -1.0,
                )
                lambda(returnData)
        }
    }
    /*open fun downloadCartItemToCache() {
        val db = Firebase.firestore
        docSnap.get().addOnSuccessListener {doc ->
            val returnData = MainListModel(
                announcementName = doc.get("Name").toString(),
                announcementDate = doc.getDate("Date") ?: Date(),
                announcementDescription = doc.get("Description").toString(),
                announcementPhotoUris = doc.get("UriArray") as ArrayList<String>,
                announcementUserId = doc.getLong("UserId")?.toInt() ?: -1,
                announcementCurrency = doc.get("Currency")?.toString()?.toInt()!!,
                announcementPrice = doc.getDouble("Price") ?: -1.0,
            )
        }
    }*/
}
