package net.dulatello08.openmarket.adapters

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.activities.PhotosPagerGroupActivity

class AddAnnouncementPhotosAdapter(private val inter: onClickdial) :
    RecyclerView.Adapter<AddAnnouncementPhotosAdapter.ViewHolder>() {
    private val list = arrayListOf<String>()

    interface onClickdial {
        fun clickDelete(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.add_announcement_photo_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.image.context).load(list[position]).into(holder.image)
        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, PhotosPagerGroupActivity::class.java)
            intent.putExtra("PhotoPosition", position)
            intent.putExtra("Photos", list)
            it.context.startActivity(intent)
        }
        holder.itemView.setOnLongClickListener {
            AlertDialog.Builder(it.context)
                .setTitle(it.context.resources.getString(R.string.delete_photo_dialog_tittle))
                .setPositiveButton(it.context.resources.getString(R.string.delete_photo_dialog_posit_button)) { dial, pos ->
                    list.removeAt(position)
                    notifyDataSetChanged()
                    inter.clickDelete(position)
                    dial.cancel()
                }.setNeutralButton("Отмена") { dial, pos ->
                }
                .setIcon(holder.image.drawable).show()
            true
        }
    }

    override fun getItemCount() = list.size
    fun setData(newList: ArrayList<String>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.addedPhotoItem)
    }
}