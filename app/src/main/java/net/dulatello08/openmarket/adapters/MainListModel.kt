package net.dulatello08.openmarket.adapters

import java.io.Serializable
import java.util.*

val CURRENCY_ARRAY = arrayListOf(
    "\u20BD", "$", "\u20B8", "сум"
)

data class MainListModel(
    val announcementName: String,
    val announcementDescription: String,
    val announcementDate: Date,
    val announcementPhotoUris: ArrayList<String>,
    val announcementUserId: Int,
    val announcementCurrency: Int,
    val announcementPrice: Double,
    val announcementId:String = ""
) : Serializable {}