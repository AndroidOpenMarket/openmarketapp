package net.dulatello08.openmarket.adapters

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.alexvasilkov.gestures.views.GestureImageView
import com.bumptech.glide.Glide

class ImageViewPager : PagerAdapter() {
    private val imageArrayList = arrayListOf<String>()
    private var onClickViewPager = {position:Int,view:View-> }
    override fun getCount(): Int {
        return imageArrayList.size
    }

    fun setData(newList: ArrayList<String>,lambda:(Int,View)->Unit) {
        imageArrayList.clear()
        imageArrayList.addAll(newList)
        onClickViewPager = lambda
        notifyDataSetChanged()
    }
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val image = GestureImageView(container.context)
        image.controller.settings.setRotationEnabled(true).isRestrictRotation = true
        image.controller.enableScrollInViewPager(container as ViewPager)
        Glide.with(container.context).load(imageArrayList[position]).into(image)
        if (image.parent != null)
            container.removeView(image)
        container.addView(image)
        image.setOnClickListener { onClickViewPager(position,image) }
        return image
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}