package net.dulatello08.openmarket.adapters

import androidx.recyclerview.widget.DiffUtil

class MainListDiffUtils(
    private val newListModel: ArrayList<MainListModel>,
    private val oldListModel: ArrayList<MainListModel>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldListModel.size

    override fun getNewListSize() = newListModel.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldListModel[oldItemPosition].announcementDate == newListModel[newItemPosition].announcementDate
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldListModel[oldItemPosition] == newListModel[newItemPosition]
    }
}