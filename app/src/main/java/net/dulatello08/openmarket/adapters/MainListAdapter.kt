package net.dulatello08.openmarket.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.activities.SelectedAnnouncementActivity
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class MainListAdapter : RecyclerView.Adapter<MainListAdapter.ViewHolder>() {
    private val modelList = arrayListOf<MainListModel>()
    private val likesList = arrayListOf<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.main_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val announcementModel = modelList[position]
        holder.adName.text = announcementModel.announcementName
        val time = Date(announcementModel.announcementDate.time)
        val timeFormat = SimpleDateFormat("HH:mm\ndd MMM", Locale.getDefault())
        holder.adDate.text = timeFormat.format(time)
        holder.adPhoto.visibility = View.GONE
        holder.adLike.setOnClickListener {
            if (it.tag == "unpressed"||it.tag==null) {
                likesList.remove(announcementModel.announcementId)
                it.setBackgroundResource(R.drawable.star_pressed_icon)
                it.tag = "pressed"
            } else if(it.tag == "pressed") {
                likesList.add(announcementModel.announcementId)
                it.setBackgroundResource(R.drawable.star_unpressed_icon)
                it.tag = "unpressed"
            }
        }
        if (announcementModel.announcementPhotoUris.size > 0) {
            holder.adPhoto.visibility = View.VISIBLE
            Glide.with(holder.adPhoto)
                .load(announcementModel.announcementPhotoUris[0])
                .centerInside().into(holder.adPhoto)
        }
        val selIntent =
            Intent(holder.itemView.context, SelectedAnnouncementActivity::class.java)
        selIntent.putExtra("Model", announcementModel)
        holder.itemView.setOnClickListener {
            it.context.startActivity(selIntent)
        }
        val dec = DecimalFormat("#,###.#")
        holder.adPrice.text = holder.itemView.context.getString(
            R.string.price_in_list_placeholder, dec.format(announcementModel.announcementPrice),
            CURRENCY_ARRAY[announcementModel.announcementCurrency]
        )
    }

    override fun getItemCount() = modelList.size
    fun getLikesId(): ArrayList<String> {
        return likesList
    }

    fun setMainData(newList: ArrayList<MainListModel>) {
        val mDiff = MainListDiffUtils(newList, modelList)
        val diffCalc = DiffUtil.calculateDiff(mDiff)
        modelList.clear()
        modelList.addAll(newList)
        diffCalc.dispatchUpdatesTo(this)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val adLike: ImageButton = itemView.findViewById(R.id.like_button_star)
        val adName: TextView = itemView.findViewById(R.id.announcementNameInList)
        val adDate: TextView = itemView.findViewById(R.id.announcementDateInList)
        val adPhoto: ImageView = itemView.findViewById(R.id.announcementPhotoInList)
        val adPrice: TextView = itemView.findViewById(R.id.announcementPriceInList)
    }
}
