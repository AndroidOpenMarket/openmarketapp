package net.dulatello08.openmarket.adapters

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import net.dulatello08.openmarket.R

class FCM : FirebaseMessagingService() {
    private fun handleMessage(remoteMessage: RemoteMessage) {
        //1
        val handler = Handler(Looper.getMainLooper())

        //2
        handler.post(Runnable {
            Toast.makeText(baseContext, "text",
                Toast.LENGTH_LONG).show()
            remoteMessage.notification?.let {
                val intent = Intent("MyData")
                intent.putExtra("message", it.body);
                broadcaster?.sendBroadcast(intent);
            }
        }
        )
    }
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        handleMessage(remoteMessage)
    }
    private var broadcaster: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
    }
}