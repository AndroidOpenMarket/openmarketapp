package net.dulatello08.openmarket.adapters

import java.util.*
import java.io.Serializable

data class ShopCartModel (
    val announcementName: String,
    val announcementDescription: String,
    val announcementDate: Date,
    val announcementPhotoUris: ArrayList<String>,
    val announcementUserId: Int,
    val announcementCurrency: Int,
    val announcementPrice: Float
) : Serializable