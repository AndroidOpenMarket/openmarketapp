package net.dulatello08.openmarket.activities

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.drawToBitmap
import androidx.viewpager.widget.ViewPager
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.ThemeAndLanguage
import net.dulatello08.openmarket.adapters.ImageViewPager

class PhotosPagerGroupActivity : AppCompatActivity() {
    private val mAdapterVp = ImageViewPager()
    private lateinit var viewPager: ViewPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(ThemeAndLanguage.NOW_THEME)
        setContentView(R.layout.activity_photos_view_pager)
        setSupportActionBar((findViewById(R.id.photosActivityToolbar)))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val photos = intent.getStringArrayListExtra("Photos") ?: arrayListOf()
        viewPager = findViewById(R.id.photosActivityPager)
        viewPager.adapter = mAdapterVp
        mAdapterVp.setData(photos) { pos, view ->
            Log.d("MyTag", "Position ${view.drawToBitmap()}")
        }
        viewPager.setCurrentItem(intent.getIntExtra("PhotoPosition", 0), true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_actionbar, menu)
        menu?.findItem(R.id.search_item)?.isVisible = true
        return super.onCreateOptionsMenu(menu)
    }
}