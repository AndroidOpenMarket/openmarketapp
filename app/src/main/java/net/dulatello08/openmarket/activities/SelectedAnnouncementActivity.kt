package net.dulatello08.openmarket.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.ThemeAndLanguage
import net.dulatello08.openmarket.adapters.CURRENCY_ARRAY
import net.dulatello08.openmarket.adapters.ImageViewPager
import net.dulatello08.openmarket.adapters.MainListModel
import net.dulatello08.openmarket.api.RandomCalls
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class SelectedAnnouncementActivity : AppCompatActivity() {
    private var mInterstitialAd: InterstitialAd? = null
    private val mAdapterVp = ImageViewPager()
    private val model: MainListModel by lazy { intent.getSerializableExtra("Model") as MainListModel }
    private val viewPager: ViewPager by lazy { findViewById(R.id.viewPagerImages) }
    private val textName: TextView by lazy { findViewById(R.id.selectedActivityName) }
    private val textDescription: TextView by lazy { findViewById(R.id.selectedActivityDescription) }
    private val textDate: TextView by lazy { findViewById(R.id.selectedActivityDate) }
    private val userName: TextView by lazy { findViewById(R.id.selectedActivityUser) }
    private val priceText: TextView by lazy { findViewById(R.id.selectedActivityPrice) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(ThemeAndLanguage.NOW_THEME)
        setContentView(R.layout.activity_selected_announcement)
        setSupportActionBar(findViewById(R.id.selectedActivityToolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewPager.adapter = mAdapterVp
        textName.text = model.announcementName
        textDescription.text = model.announcementDescription
        val dec = DecimalFormat("#,###.#")
        priceText.text = getString(
            R.string.price_in_list_placeholder, dec.format(model.announcementPrice),
            CURRENCY_ARRAY[model.announcementCurrency]
        )
        Firebase.firestore.collection("Users").document(model.announcementUserId.toString()).get()
            .addOnSuccessListener {
                userName.text = it.getString("username")
                fullscreenAd()
            }
        val timeFormat = SimpleDateFormat("dd MMMM   HH:mm", Locale.getDefault())
        textDate.text = timeFormat.format(model.announcementDate)
        mAdapterVp.setData(model.announcementPhotoUris) { position, _ ->
            val photosIntent = Intent(this, PhotosPagerGroupActivity::class.java)
            photosIntent.putStringArrayListExtra("Photos", model.announcementPhotoUris)
            photosIntent.putExtra("PhotoPosition", viewPager.currentItem)
            startActivity(photosIntent)
        }
        findViewById<ImageButton>(R.id.imageButton).setOnClickListener {

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun fullscreenAd() {
        Log.d("MyTag", "Ad request")
        val adRequest = AdRequest.Builder().build()
        val mRandomCalls = RandomCalls()
        InterstitialAd.load(
            this,
            "ca-app-pub-7509088958653785/9139125550",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    mInterstitialAd = null
                    Log.d("MyTag", "Ad fail")
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d("MyTag", "Ad load")
                    mInterstitialAd = interstitialAd
                    if (mRandomCalls.getRandomBoolean(2)) {
                        mInterstitialAd?.show(this@SelectedAnnouncementActivity)
                    }
                }
            })
        mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {
                mInterstitialAd = null
            }
        }
    }
}