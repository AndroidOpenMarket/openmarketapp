package net.dulatello08.openmarket.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.ThemeAndLanguage

const val SHARED_PREFERENCE_NAME = "OPEN_MARKET_PREFERENCES"

class LoginActivity : AppCompatActivity() {
    private val RC_SIGN_IN = 154
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var auth: FirebaseAuth
    private lateinit var regBtn: ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(ThemeAndLanguage.NOW_THEME)
        setContentView(R.layout.activity_login)
        auth = Firebase.auth
        val currentUser = auth.currentUser
        regBtn = findViewById(R.id.singIn)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.def_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
        regBtn.setOnClickListener {
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        updateUI(currentUser)
    }

    override fun onBackPressed() {
        if (Firebase.auth.currentUser == null)
            Toast.makeText(this, "First Login", Toast.LENGTH_SHORT).show()
        else
            finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {

                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                Log.d("MyTag","ApiException")
            }
        }
    }

    private fun updateUI(userInfo: FirebaseUser?) {
        if (userInfo != null) {
            doIt()
            finish()
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnSuccessListener {
                // Sign in success, update UI with the signed-in user's information
                val user = auth.currentUser
                updateUI(user)
            }

    }

    private fun createUserDataDocument(onAddedUser: (HashMap<String, Any>) -> Unit) {
        val emailNotParsed = Firebase.auth.currentUser?.email
        val username = emailNotParsed?.substring(0, emailNotParsed.length - 10)
        val db = Firebase.firestore
        val ref = db.collection("Users")
        val idRef = ref.orderBy("id", Query.Direction.DESCENDING).limit(1)
        idRef.get().addOnSuccessListener {
            val id = if (it.size() > 0)
                (it.documents[0].id.toInt() + 1)
            else 0
            val userData = hashMapOf<String, Any>(
                "username" to username.toString(),
                "id" to id,
            )
            db.collection("Users").document(id.toString()).set(userData)
                .addOnSuccessListener {
                    onAddedUser(userData)
                }
        }
    }

    private fun checkId(lambda: (String, Int) -> Unit) {
        val db = Firebase.firestore
        val ref = db.collection("Users")
        val emailNotParsed = Firebase.auth.currentUser?.email
        val username = emailNotParsed?.substring(0, emailNotParsed.length - 10)
        ref.whereEqualTo("username", username).limit(1).get().addOnSuccessListener { query ->
            if (query.documents.size <= 0)
                createUserDataDocument { lambda(it["username"].toString(), it["id"] as Int) }
            else
                lambda(
                    query.documents[0].getString("username").toString(),
                    query.documents[0].getLong("id")?.toInt() ?: -1
                )
        }
    }

    private fun doIt() {
        val sharedPref = getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        checkId { userName, userId ->
            Log.d("MyTag","User id $userId")
            with(sharedPref.edit()) {
                putInt("id", userId)
                apply()
            }
        }
    }
}