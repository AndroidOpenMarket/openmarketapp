package net.dulatello08.openmarket.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import net.dulatello08.openmarket.R
import net.dulatello08.openmarket.ThemeAndLanguage
import net.dulatello08.openmarket.fragments.AddAnnouncementFragment
import net.dulatello08.openmarket.fragments.HomeFragment
import net.dulatello08.openmarket.fragments.SettingsFragment
import net.dulatello08.openmarket.fragments.ShoppingCartFragment
import java.util.*

class MainActivity : AppCompatActivity() {
    private val mHomeFragment = HomeFragment()
    private val mSettingsFragment = SettingsFragment()
    private val mShoppingCartFragment = ShoppingCartFragment()
    private val mAddAnnouncementFragment = AddAnnouncementFragment()
    private var nowFragment: Fragment = mHomeFragment
    private lateinit var bottomNav: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Firebase.auth.currentUser == null) startActivity(
            Intent(
                this,
                LoginActivity::class.java
            )
        )
        setThemeAndLang()
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.mainToolbar))
        MobileAds.initialize(this)
        bottomNav = findViewById(R.id.bottomNavView)
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, mHomeFragment)
            .add(R.id.fragmentContainer, mShoppingCartFragment)
            .add(R.id.fragmentContainer, mSettingsFragment)
            .add(R.id.fragmentContainer, mAddAnnouncementFragment)
            .hide(mShoppingCartFragment)
            .hide(mSettingsFragment)
            .hide(mAddAnnouncementFragment).commit()
        bottomNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> switchFragment(mHomeFragment)
                R.id.navigation_shoppingCart -> switchFragment(mShoppingCartFragment)
                R.id.navigation_settings -> switchFragment(mSettingsFragment)
                R.id.navigation_addAd -> switchFragment(mAddAnnouncementFragment)
            }
            true
        }
    }

    private fun setThemeAndLang() {
        val shr = getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        ThemeAndLanguage.NOW_THEME = ThemeAndLanguage.ALL_THEMES[shr.getInt("THEME", 0)]
        setTheme(ThemeAndLanguage.NOW_THEME)
        val config = resources.configuration
        val lang = when (shr.getInt("LANG", 0)) {
            0 -> "en"
            1 -> "ru"
            else -> "en"
        }
        val locale = Locale(lang)
        Locale.setDefault(locale)
        config.setLocale(locale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            createConfigurationContext(config)
        resources.updateConfiguration(config, resources.displayMetrics)
    }

    private fun switchFragment(fragment: Fragment) {
        if (fragment != nowFragment) {
            supportFragmentManager.beginTransaction().show(fragment).hide(nowFragment).commit()
            nowFragment = fragment
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_actionbar, menu)
        if (menu != null) {
            val search = menu.findItem(R.id.search_item)?.actionView as SearchView
            search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    mHomeFragment.search(query ?: "")
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    mHomeFragment.search(newText ?: "")
                    return true
                }
            })
        }
        return super.onCreateOptionsMenu(menu)
    }
}