package net.dulatello08.openmarket

class ThemeAndLanguage {
    companion object {
        val ALL_THEMES = arrayListOf(R.style.LightTheme, R.style.DarkTheme)
        var NOW_THEME = R.style.LightTheme
    }
}